import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { throwError } from 'rxjs';
import { PostModel } from 'src/app/shared/post-model';
import { PostService } from 'src/app/shared/post.service';
import { CommentPayload } from 'src/app/comment/comment.payload';
import { CommentService } from 'src/app/comment/comment.service';

@Component({
  selector: 'app-view-post',
  templateUrl: './view-post.component.html',
  styleUrls: ['./view-post.component.scss'],
})
export class ViewPostComponent implements OnInit {
  postId: number;
  post!: PostModel;
  commentForm: FormGroup;
  commentPayload: CommentPayload;
  comments?: CommentPayload[];

  constructor(
    private postService: PostService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private commentService: CommentService
  ) {

    this.postId = this.activatedRoute.snapshot.params['id'];

    this.commentForm = new FormGroup({
      text: new FormControl('', Validators.required),
    });

    this.commentPayload = {
      text: '',
      postId: this.postId,
    };
    
    this.getPostById();
    this.getCommentsForPost();
  }

  ngOnInit(): void {
    
  }

  postComment() {
    this.commentPayload.text = this.commentForm.get('text')?.value;
    this.commentService.postComment(this.commentPayload).subscribe((data) => {
      this.commentForm.get('text')?.setValue('');
      this.getCommentsForPost();
    }, (error) => {
      throwError(error);
    })
  }

  private getPostById() {
    this.postService.getPost(this.postId).subscribe((data) => {
      this.post = data;
    }, (error) => {
      throwError(error);
    })
  }

  getCommentsForPost() {
    this.commentService.getAllCommentsForPost(this.postId).subscribe((data) => {
      this.comments = data;
    }, (error) => {
      throwError(error);
    });
  }
}
