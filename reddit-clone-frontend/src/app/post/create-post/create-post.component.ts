import { throwError } from 'rxjs';
import { SubredditService } from './../../subreddit/subreddit.service';
import { PostService } from './../../shared/post.service';
import { CreatePostPayload } from './create-post.payload';
import { Component, OnInit } from '@angular/core';
import { SubredditModel } from 'src/app/subreddit/subreddit-model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.scss'],
})
export class CreatePostComponent implements OnInit {
  createPostForm: FormGroup;
  postPayload: CreatePostPayload;
  subreddits?: Array<SubredditModel>;

  constructor(
    private router: Router,
    private subredditService: SubredditService,
    private postService: PostService
  ) {
    this.createPostForm = new FormGroup({
      postName: new FormControl('', Validators.required),
      subredditName: new FormControl('', Validators.required),
      url: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
    });

    this.postPayload = {
      postName: '',
      url: '',
      description: '',
      subredditName: '',
    };
  }

  ngOnInit(): void {
    this.subredditService.getAllSubreddits().subscribe(
      (data) => {
        this.subreddits = data;
      },
      (error) => {
        throwError(error);
      }
    );
  }

  createPost() {
    this.postPayload.postName = this.createPostForm.get('postName')?.value;
    this.postPayload.subredditName = this.createPostForm.get('subredditName')?.value;
    this.postPayload.url = this.createPostForm.get('url')?.value;
    this.postPayload.description = this.createPostForm.get('description')?.value;

    this.postService.createPost(this.postPayload).subscribe(() => {
      this.router.navigateByUrl('/');
    },
    (error) => {
      throwError(error);
    });
  }

  discardPost() {
    this.router.navigateByUrl('/');
  }
}
