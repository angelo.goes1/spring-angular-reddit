import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { PostModel } from 'src/app/shared/post-model';
import { PostService } from 'src/app/shared/post.service';

@Component({
  selector: 'app-subreddit-profile',
  templateUrl: './subreddit-profile.component.html',
  styleUrls: ['./subreddit-profile.component.scss'],
})
export class SubredditProfileComponent implements OnInit {
  
  id!: number;
  posts?: PostModel[];

  constructor(
    private postService: PostService,
    private activatedRoute: ActivatedRoute
  ) {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.postService.getAllPostsBySubreddit(this.id).subscribe((data) => {
      this.posts = data;
    });
  }

  ngOnInit(): void {}
}
