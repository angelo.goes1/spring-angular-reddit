import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubredditProfileComponent } from './subreddit-profile.component';

describe('SubredditProfileComponent', () => {
  let component: SubredditProfileComponent;
  let fixture: ComponentFixture<SubredditProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubredditProfileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubredditProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
