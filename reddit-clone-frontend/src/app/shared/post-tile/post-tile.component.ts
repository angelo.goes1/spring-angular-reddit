import { Router } from '@angular/router';
import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { PostModel } from '../post-model';
import { faComments } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-post-tile',
  templateUrl: './post-tile.component.html',
  styleUrls: ['./post-tile.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PostTileComponent implements OnInit {
  @Input() posts?: Array<PostModel>;
  faComments = faComments;

  constructor(private router: Router) {}

  ngOnInit(): void {}

  goToPost(id: number){
    this.router.navigateByUrl('/view-post/' + id);
  }
}
