export enum VoteType {
    UPVOTE,
    DOWNVOTE,
    UNDEFINED
}