import { PostService } from './../shared/post.service';
import { Component, OnInit } from '@angular/core';
import { PostModel } from '../shared/post-model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  posts$: Array<PostModel> = [];

  constructor(private postService: PostService) {
    this.postService.getAllPosts().subscribe(post => {
      // console.log('pegou posts');
      this.posts$ = post;
    });
  }

  ngOnInit() {
  }

}
