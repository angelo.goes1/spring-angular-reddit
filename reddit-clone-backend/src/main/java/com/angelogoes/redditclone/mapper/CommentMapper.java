package com.angelogoes.redditclone.mapper;

import java.time.Instant;

import org.springframework.stereotype.Service;

import com.angelogoes.redditclone.dto.CommentsDto;
import com.angelogoes.redditclone.model.Comment;
import com.angelogoes.redditclone.model.Post;
import com.angelogoes.redditclone.model.User;

@Service
public class CommentMapper {
    public Comment map(CommentsDto commentsDto, Post post, User user) {
        return Comment.builder()
                //.id(commentsDto.getId())
                .text(commentsDto.getText())
                .createdDate(Instant.now())
                .post(post)
                .user(user)
                .build();
    }

    public CommentsDto mapToDto(Comment comment) {
        return CommentsDto.builder()
                .id(comment.getId())
                .postId(comment.getPost().getPostId())
                .createdDate(comment.getCreatedDate())
                .text(comment.getText())
                .userName(comment.getUser().getUsername())
                .build();
    }
}
