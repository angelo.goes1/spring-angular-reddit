package com.angelogoes.redditclone.mapper;

import java.time.Instant;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.angelogoes.redditclone.dto.PostRequest;
import com.angelogoes.redditclone.dto.PostResponse;
import com.angelogoes.redditclone.model.Post;
import com.angelogoes.redditclone.model.Subreddit;
import com.angelogoes.redditclone.model.User;
import com.angelogoes.redditclone.model.Vote;
import com.angelogoes.redditclone.model.VoteType;
import com.angelogoes.redditclone.repository.CommentRepository;
import com.angelogoes.redditclone.repository.VoteRepository;
import com.angelogoes.redditclone.service.AuthService;
import com.github.marlonlom.utilities.timeago.TimeAgo;

import lombok.AllArgsConstructor;

import static com.angelogoes.redditclone.model.VoteType.DOWNVOTE;
import static com.angelogoes.redditclone.model.VoteType.UPVOTE;

@Service
@AllArgsConstructor
public class PostMapper {

    private final CommentRepository commentRepository;
    private final AuthService authService;
    private final VoteRepository voteRepository;

    public Post map(PostRequest postRequest, Subreddit subreddit, User user) {
        return Post.builder()
                .createdDate(Instant.now())
                .postName(postRequest.getPostName())
                .description(postRequest.getDescription())
                .url(postRequest.getUrl())
                .subreddit(subreddit)
                .voteCount(0)
                .user(user)
                .build();
    }

    public PostResponse mapToDto(Post post) {
        return PostResponse.builder()
                .id(post.getPostId())
                .postName(post.getPostName())
                .description(post.getDescription())
                .url(post.getUrl())
                .voteCount(post.getVoteCount())
                .subredditName(post.getSubreddit().getName())
                .userName(post.getUser().getUsername())
                .commentCount(commentCount(post))
                .duration(getDuration(post))
                .upVote(isPostUpVoted(post))
                .downVote(isPostDownVoted(post))
                .build();
    }

    Integer commentCount(Post post) {
        return commentRepository.findByPost(post).size();
    }

    String getDuration(Post post) {
        return TimeAgo.using(post.getCreatedDate().toEpochMilli());
    }

    boolean isPostUpVoted(Post post) {
        return checkVoteType(post, UPVOTE);
    }

    boolean isPostDownVoted(Post post) {
        return checkVoteType(post, DOWNVOTE);
    }

    private boolean checkVoteType(Post post, VoteType voteType) {
        if (authService.isLoggedIn()) {
            Optional<Vote> voteForPostByUser = voteRepository.findTopByPostAndUserOrderByVoteIdDesc(post,
                    authService.getCurrentUser());
            return voteForPostByUser.filter(vote -> vote.getVoteType().equals(voteType))
                    .isPresent();
        }
        return false;
    }
}
