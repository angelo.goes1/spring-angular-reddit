package com.angelogoes.redditclone.mapper;

import org.springframework.stereotype.Service;

import com.angelogoes.redditclone.dto.SubredditDto;
import com.angelogoes.redditclone.model.Subreddit;

@Service
public class SubredditMapper {

    public SubredditDto mapSubredditToDto(Subreddit subreddit) {
        return SubredditDto.builder()
                .name(subreddit.getName())
                .id(subreddit.getId())
                .description(subreddit.getDescription())
                .numberOfPosts(subreddit.getPosts().size())
                .build();
    }

    public Subreddit mapDtoToSubreddit(SubredditDto subredditDto) {
        return Subreddit.builder()
                .name(subredditDto.getName())
                .description(subredditDto.getDescription())
                .build();
    }
}