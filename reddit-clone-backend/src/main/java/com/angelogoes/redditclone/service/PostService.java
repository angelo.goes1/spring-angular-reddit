package com.angelogoes.redditclone.service;

import java.util.List;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.angelogoes.redditclone.dto.PostRequest;
import com.angelogoes.redditclone.dto.PostResponse;
import com.angelogoes.redditclone.exceptions.PostNotFoundException;
import com.angelogoes.redditclone.exceptions.SubredditNotFoundException;
import com.angelogoes.redditclone.mapper.PostMapper;
import com.angelogoes.redditclone.model.Post;
import com.angelogoes.redditclone.model.Subreddit;
import com.angelogoes.redditclone.model.User;
import com.angelogoes.redditclone.repository.PostRepository;
import com.angelogoes.redditclone.repository.SubredditRepository;
import com.angelogoes.redditclone.repository.UserRepository;

import static java.util.stream.Collectors.toList;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@AllArgsConstructor
@Transactional
@Slf4j
public class PostService {
    
    private final SubredditRepository subredditRepository;
    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final AuthService authService;
    private final PostMapper postMapper;

    public Post save(PostRequest postRequest) {
        Subreddit subreddit = subredditRepository.findByName(postRequest.getSubredditName())
                .orElseThrow(() -> new SubredditNotFoundException(postRequest.getSubredditName()));
        User currentUser = authService.getCurrentUser();
        Post newPost = postMapper.map(postRequest, subreddit, currentUser);

        log.info(newPost.getPostName());

        return postRepository.save(newPost);
    }

    @Transactional(readOnly = true)
    public List<PostResponse> getAllPosts() {
        return postRepository.findAll()
                .stream()
                .map(postMapper::mapToDto)
                .collect(toList());
    }

    @Transactional(readOnly = true)
    public PostResponse getPost(Long id) {
        Post post = postRepository.findById(id)
                .orElseThrow(() -> new PostNotFoundException(id.toString()));
        return postMapper.mapToDto(post);
    }

    @Transactional(readOnly = true)
    public List<PostResponse> getPostsBySubreddit(Long id) {
        Subreddit subreddit = subredditRepository.findById(id)
            .orElseThrow(() -> new SubredditNotFoundException(id.toString()));
        List<Post> posts = postRepository.findAllBySubreddit(subreddit);
        return posts.stream()
            .map(postMapper::mapToDto)
            .collect(toList());
    }

    @Transactional(readOnly = true)
    public List<PostResponse> getPostsByUsername(String username) {
        User user = userRepository.findByUsername(username)
        .orElseThrow(() -> new UsernameNotFoundException(username));

        return postRepository.findByUser(user)
            .stream()
            .map(postMapper::mapToDto)
            .collect(toList());
        
    } 

}
