package com.angelogoes.redditclone.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.angelogoes.redditclone.dto.VoteDto;
import com.angelogoes.redditclone.exceptions.PostNotFoundException;
import com.angelogoes.redditclone.exceptions.SpringRedditException;
import com.angelogoes.redditclone.model.Post;
import com.angelogoes.redditclone.model.Vote;
import com.angelogoes.redditclone.repository.PostRepository;
import com.angelogoes.redditclone.repository.VoteRepository;

import lombok.AllArgsConstructor;

import static com.angelogoes.redditclone.model.VoteType.UPVOTE;

@Service
@AllArgsConstructor
public class VoteService {

    private final VoteRepository voteRepository;
    private final PostRepository postRepository;
    private final AuthService authService;

    @Transactional
    public void vote(VoteDto voteDto) {
        Post post = postRepository.findById(voteDto.getPostId())
                .orElseThrow(() -> new PostNotFoundException("Post Not Found with ID - " + voteDto.getPostId()));
        Optional<Vote> voteByPostAndUser = voteRepository.findTopByPostAndUserOrderByVoteIdDesc(post,
                authService.getCurrentUser());
        if (voteByPostAndUser.isPresent() &&
                voteByPostAndUser.get()
                        .getVoteType()
                        .equals(voteDto.getVoteType())) {
            throw new SpringRedditException("You already "
                    + voteDto.getVoteType() + "'d this post");
        }
        if (UPVOTE.equals(voteDto.getVoteType())) {
            post.setVoteCount(post.getVoteCount() + 1);
        } else {
            post.setVoteCount(post.getVoteCount() - 1);
        }

        voteRepository.save(mapToVote(voteDto, post));
        postRepository.save(post);
    }

    private Vote mapToVote(VoteDto voteDto, Post post) {
        return Vote.builder()
                .voteType(voteDto.getVoteType())
                .post(post)
                .user(authService.getCurrentUser())
                .build();
    }

}
