package com.angelogoes.redditclone.repository;

import com.angelogoes.redditclone.model.Comment;
import com.angelogoes.redditclone.model.Post;
import com.angelogoes.redditclone.model.User;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

    List<Comment> findByPost(Post post);

    List<Comment> findByUser(User user);
}