package com.angelogoes.redditclone.repository;

import com.angelogoes.redditclone.model.Post;
import com.angelogoes.redditclone.model.User;
import com.angelogoes.redditclone.model.Vote;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VoteRepository extends JpaRepository<Vote, Long> {

    Optional<Vote> findTopByPostAndUserOrderByVoteIdDesc(Post post,
            User currentUser);
}