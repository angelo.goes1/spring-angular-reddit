package com.angelogoes.redditclone.repository;

import com.angelogoes.redditclone.model.Post;
import com.angelogoes.redditclone.model.Subreddit;
import com.angelogoes.redditclone.model.User;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

    List<Post> findAllBySubreddit(Subreddit subreddit);

    List<Post> findByUser(User user);
}